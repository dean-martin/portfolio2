<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Project extends Model
{
    /**
     * @return BelongsToMany
     */
   public function tags()
   {
       return $this->belongsToMany('App\Tag');
   }
}
