<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class RegisterUserRoles extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register user roles into the database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(['owner', 'admin'] as $role){
            $this->line("Checking {$role}");
            if(!Role::where('name', $role)->exists()){
                $this->info("Creating {$role} role.");
                Role::create(['name' => $role]);
            }
        }   
    }
}
