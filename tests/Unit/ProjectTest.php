<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProjectTest extends TestCase
{
    use DatabaseMigrations;
   
    /** @test */
    function it_belongs_to_many_tags()
    {
        $project = create('App\Project');
        $tags = factory('App\Tag', 2)->create();

        $project->tags()->attach($tags);

        $this->assertInstanceOf(\Illuminate\Support\Collection::class, $project->tags);
    
        $this->assertEquals($tags->pluck('name'), $project->tags->pluck('name'));
    }
}
