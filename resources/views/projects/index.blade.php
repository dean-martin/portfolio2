@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="">
            <div class="card">
                <div class="card-header">Projects</div>

                <div class="card-body">
                   @foreach($projects as $project)
                    <div class="jumbotron">
                        <h1 class="display-4">{{$project->name}}</h1>
                        <p class="lead">{{$project->description}}</p>
                         <hr class="my-4">
                        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
                        <a class="btn btn-primary btn-lg"
                           href="{{route('projects.show', $project->id)}}"
                           role="button">View</a>
                    </div>
                   @endforeach  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
