<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{env('APP_NAME', 'Dean Martin')}}</title>


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    @if(app()->environment() == 'production' && env('GOOGLE_ANALYTICS_ID') !== '')
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async
            src="https://www.googletagmanager.com/gtag/js?id={{env('GOOGLE_TRACKING_ID')}}"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{env('GOOGLE_TRACKING_ID')}}');
        </script>
    @endif

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  </head>
    
<body id="app">
    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>Dean Martin</strong>
            </h1>
            <h2>Professional Software Developer</h2>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            {{-- <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a> --}}
                <a href="https://www.github.com/dean-martin" aria-label="GitHub"><i class="fab fa-github"></i></a>
                <a href="https://www.linkedin.com/in/deanwmartin"
                    aria-label="LinkedIn"><i class="fab fa-linkedin"></i></a>

<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mr-auto text-center" style="margin:0 auto;">
        <i class="fas fa-envelope fa-3x mb-3 sr-contact-2"></i>
        <p>
        <a href="mailto:contact@deanmartin.pro"><strong>contact@deanmartin.pro</strong></a>
        </p>
      </div>
    </div>
  </div>
</section>
        </div> 
      </div>
    </header>
    @yield('content')
</body>
</html>
