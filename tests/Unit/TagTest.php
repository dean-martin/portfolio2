<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TagTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function it_belongs_to_many_projects()
    {
        $tag = create('App\Tag');            
        $projects = factory('App\Project', 5)->create();
        $tag->projects()->attach($projects);

        $this->assertInstanceOf('Illuminate\Support\Collection', $tag->projects);
        $this->assertEquals($tag->projects->pluck('name'), $projects->pluck('name'));
    }
}
