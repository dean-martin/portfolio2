<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = create('App\User', ['name' => 'owner', 'email' => 'owner@website.com']);
        $user->assignRole('owner');

        factory('App\User', 50)->create();
    }
}
