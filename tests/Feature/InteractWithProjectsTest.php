<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class InteractWithProjectsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }

    /** @test */
    function guests_can_not_create_projects()
    {
        $this->withoutExceptionHandling();
        $this->expectException(AuthenticationException::class);
        $this->get(route('projects.create'));
    }

    /** @test */
    function admins_can_create_projects()
    {
        $this->withoutExceptionHandling();
        $user = create('App\User');
        $user->assignRole('owner');
        $this->signIn($user); 
        // TODO(vulski): FIX ME!!!!!!!!
        $this->get(route('projects.create'))->assertSee('');
    }

}
