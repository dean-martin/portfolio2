<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(['owner', 'admin'] as $role){
            if(!Role::where('name', $role)->exists()){
                Role::create(['name' => $role]);
            }
        }   
    }
}
